{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = "Multiple Matches"}
    {$PageId = $JobMatchesPage}
{/block}


{block name=scripts}

{*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
<script type="text/javascript" src="{$_subdomain}/js/job.form.validation.js"></script> 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>

<script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>


<script type="text/javascript" charset="utf-8">    


function updateFn($sRow) { 
    if(!isNaN($sRow[2])) {     
        document.location = '{$_subdomain}/index/jobupdate/'+ $sRow[2] ;
    }
}

$(document).ready(function() {

//    if(getUrlVars()["jobSearch"]) {
//	$("#jobSearch").val(getUrlVars()["jobSearch"]);
//    }


    /*$.post('{$_subdomain}/Job/getServiceCentersFromSearch/' + 
	    getUrlVars()["jobSearch"] + '/' + {$activeFilter|default:"''"} + '/' + 
	    {$filterID|default:"''"} + '/' + getUrlVars()["batchID"], 
	    { }, 
	    function(response) {
		var sc = $.parseJSON(response);
		var html = "<option value='' {if $serviceProviderID == ''}selected='selected'{/if}>Select Provider</option>";
		for(var i = 0; i < sc.length; i++) {
		    html += "<option value=''" + (('{$serviceProviderID}' == sc[i].ServiceProviderID) ? "selected='selected'" : "") + ">" + sc[i].CompanyName + "</option>";
		}
		$("#serviceProviderID").html(html);
	    }
    );*/


    $(document).on("change","#networkID", function() {
	$("#networkSelect").submit();
    });

    $(document).on("change","#clientID", function() {
	$("#clientSelect").submit();
    });

    $(document).on("change","#branchID", function() {
	$("#branchSelect").submit();
    });

    $(document).on("change","#serviceProviderID", function() {
	$("#serviceSelect").submit();
    });

    /*
    var oMatchesTable = $('#matches_table').PCCSDataTable( {
        bAutoWidth:	    false,
	displayButtons:     'P',
        permission:         'R',
	aoColumns: [
	    { sWidth: "7%" },
	    { sWidth: "8%", sType: "date-eu"},
	    { sWidth: "6%" },
	    { sWidth: "8%" },
	    { sWidth: "15%" },
	    { sWidth: "8%" },
	    { sWidth: "13%" },
	    { sWidth: "9%" },
	    { sWidth: "17%" },
	    { sWidth: "5%" },
	    { sWidth: "4%", bSortable: false }
	],
        htmlTableId:        'matches_table',
        pickButtonId:       'job_PickButton',
        viewButtonId:       'job_view_but',
        pickCallbackMethod: 'updateFn',
        dblclickCallbackMethod: 'updateFn',
        colorboxFormId:     'jobForm',
        fetchDataUrl:       '{$_subdomain}/Data/jobs/search/' + getUrlVars()["jobSearch"] + '/' + {$activeFilter|default:"''"} + '/' + {$filterID|default:"''"} + '/' + getUrlVars()["batchID"],
        parentURL:          '',
        searchCloseImage:   '{$_subdomain}/css/Skins/{$_theme}/images/close.png',
        frmErrorMsgClass:   'webformerror',
        frmErrorElement:    'label',
        suggestTextId:	    'suggestText',
        sugestFieldId:	    'JobID',
        frmErrorSugMsgClass: 'suggestwebformerror',
        hiddenPK:	    'pk',
        passParamsType:	    '1', //1 'means' parameters in query string. Default 0; 
	//superFilter :	    {$Job->super_filter},
        pickDataIdList:	    new Array('#JobID'),
        pickButtonText:	    'Update',
	fnRowCallback: function() { console.log("row"); }
    } );
    */
    
    oMatchesTable = $("#matches_table").dataTable({
	bAutoWidth:	false,
	aoColumns: [
	    { sWidth: "7%" },
	    { sWidth: "8%", sType: "date-eu"},
	    { sWidth: "6%" },
	    { sWidth: "8%" },
	    { sWidth: "15%" },
	    { sWidth: "8%" },
	    { sWidth: "13%" },
	    { sWidth: "9%" },
	    { sWidth: "17%" },
	    { sWidth: "5%" },
	    { sWidth: "4%", bSortable: false }
	],
	bDestroy:           true,
	bStateSave:         true,
	bServerSide:        true,
	bProcessing:        true,
	htmlTableId:        "matches_table",
	sDom:               "ft<'#dataTables_child'>Trpli",
	sPaginationType:    "full_numbers",
	bPaginate:          true,
	bSearch:            false,
        iDisplayLength:     10,
        "aaSorting": [[ 0, "desc" ]],
	sAjaxSource:        '{$_subdomain}/Data/jobs/search/' + getUrlVars()["jobSearch"] + '/' + {$activeFilter|default:"''"} + '/' + {$filterID|default:"''"} + '/' + getUrlVars()["batchID"],
	oTableTools: {
	    sRowSelect: "single",
	    aButtons: [
		{
		    sExtends:	"text",
		    sButtonText:    "View Job Details",
		    fnClick: function() { 
			var tbl = TableTools.fnGetInstance("matches_table");
			var data = tbl.fnGetSelectedData();
			location.assign("{$_subdomain}/index/jobupdate/" + data[0][0] + "/?ref=jobSearch");
		    }
		}
	    ],
	    fnRowSelected: function(node) { /*selectStatus(node);*/ }
	},
	oLanguage: {
	    sSearch: "<span id='searchLabel' style='float:left; top:10px; position:relative;'>Search within results:</span>&nbsp;"
	},
	fnInitComplete: function() {
	    var html = '<a href="#" id="delSearch" style="top:14px; right:4px; position:absolute;">\
			    <img style="position:relative; zoom:1; bottom:4px;" src="{$_subdomain}/css/Skins/skyline/images/close.png">\
			</a>';
	    $(html).insertAfter(".dataTables_filter input");
	    $(document).on("click", "#delSearch", function() {
		$(".dataTables_filter input").val("");
		oMatchesTable.fnFilter("");
		return false;
	    });
	},
	fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
	    if(parseInt(aData[3]) > parseInt(aData[11])) {
		var cell = $(nRow).find("td").toArray()[3];
		$(cell).html("<span style='color:red; font-weight:bold'>" + $(cell).html() + "</span>");
	    }
	    //var cell = $(nRow).find("td").toArray()[9];
	    //var html = "<input type='checkbox' value='" + aData[0] + "' />";
	    //$(cell).html(html);
	}
    });


    $(document).on("dblclick", "tr", function() {
	location.assign("{$_subdomain}/index/jobupdate/" + oMatchesTable.fnGetData(this)[0] + "/?ref=jobSearch");
    });

    
});

</script>

{/block}


{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">Home Page</a> / Job Search
    </div>
</div>
        
<div class="main" id="jobmatches">   
   
    <div class="SearchPanel">
        {include file='include/job_search.tpl'}
    </div> 
             
    <hr />
   
    
    <div id="jobSearchDropdowns">
	
	{if $userType=="Admin"}
	    {if isset($networks) and $networks|@count>1}
		<form id="networkSelect" class="inline" action="" method="post">
		    <input type="hidden" name="activeFilter" value="network" />
		    <select name="networkID" id="networkID" >
			<option value="" {if $nId eq ''}selected="selected"{/if}>{*$page['Text']['select_service_network']|escape:'html'*}Select Network</option>
			{foreach $networks as $network}
			    <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>
			{/foreach}
		    </select>
		</form>
	    {/if}
	{/if}
	
	{if $userType=="Admin" || $userType=="Network"}
	    {if isset($clients) and $clients|@count>1}
		<form id="clientSelect" class="inline" action="" method="post">
		    <input type="hidden" name="activeFilter" value="client" />
		    {if isset($post['networkID'])}
			<input type="hidden" name="networkID" value="{$post['networkID']}" />
		    {/if}
		    <select name="clientID" id="clientID" >
			<option value="" {if $cId eq ''}selected="selected"{/if}>{*$page['Text']['select_client']|escape:'html'*}Select Client</option>
			{foreach $clients as $client}
			    <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>
			{/foreach}
		    </select>
		</form>
	    {/if}
	{/if}
	    
	{if $userType=="Admin" || $userType=="Network" || $userType=="Client"}
	    {if isset($branches) and $branches|@count>1}
		<form id="branchSelect" class="inline" action="" method="post">    
		    <input type="hidden" name="activeFilter" value="branch" />
		    {if isset($post['networkID'])}
			<input type="hidden" name="networkID" value="{$post['networkID']}" />
		    {/if}
		    {if isset($post['clientID'])}
			<input type="hidden" name="clientID" value="{$post['clientID']}" />
		    {/if}
		    <select name="branchID" id="branchID" >
			<option value="" {if $bId eq ''}selected="selected"{/if}>{*$page['Text']['select_branch']|escape:'html'*}Select Branch</option>
			{foreach $branches as $branch}
			    <option value="{$branch.BranchID}" {if $bId eq $branch.BranchID}selected="selected"{/if}>{$branch.BranchName|escape:'html'}</option>
			{/foreach}
		    </select>                        
		</form>
	    {/if}
	{/if}
{if $userType!="ServiceProvider"} {* usertype is not equal to ServiceProvider its shows the select provider box ---  added by thirumal  *}
    
	<form id="serviceSelect" action="" method="post">
	    <input type="hidden" name="activeFilter" value="service" />
	    <select name="serviceProviderID" id="serviceProviderID">
		<option value="" {if $serviceProviderID == ""}selected="selected"{/if}>Select Provider</option>
		{foreach $serviceProviders as $service}
		    <option value="{$service.ServiceProviderID}" {if $serviceProviderID==$service.ServiceProviderID}selected="selected"{/if}>{$service.CompanyName}</option>
		{/foreach}
	    </select>
	</form>
	{/if}
    </div>

    
    
    
   {* {if $Job->search_result != ''} *}
       
    <table class="browse dataTable" id="matches_table" style="width:100%;">
	<thead>
	    <th title="Skyline job number">SL No</th>
	    <th title="Job booked date">Booked</th>
	    <th title="Days job old">Days</th>
	    <th title="Overdue days">Overdue</th>
	    <th title="Service Provider Name">Service Provider</th>
	    <th title="Service Provider Job Number">Job No</th>
	    <th title="Consumer">Consumer</th>
	    <th title="Manufacturer">Post Code</th>
	    <th title="Job Status">Status</th>
	    <th title="Field Service">F/S</th>
	    <th title=""></th>
	</thead>
    </table>
        
  {include file='include/button_panel.tpl'} 
  
 {* {/if} *}
                                    
</div>
{/block}
