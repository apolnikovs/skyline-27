{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $AdminHomePage}
{/block}


{block name=scripts}

<script type="text/javascript">


    var html = '<fieldset>\
		    <legend>Jobs Booked</legend>\
		    <p style="font-size:14px; text-align:center; margin:10px 0px 10px 0px;">Select date range</p>\
		    <label style="width:80px;" for="dateFrom">Date From:</label>\
		    <input type="text" id="dateFrom" />\
		    <label style="width:80px;" for="dateTo">Date To:</label>\
		    <input type="text" id="dateTo" />\
		    <p style="text-align:center; margin-bottom:0; padding-bottom:0;">\
			<button id="generate" style="color:#fff; float:none; margin-top:10px;" class="gplus-blue">Generate</button>\
		    </p>\
		</fieldset>\
	       ';
    
    $(document).on("click", "#jobsBooked", function() {
	$.colorbox({    html :	html, 
			title :	"Jobs Booked", 
			scrolling : false,
			onComplete: function() {  },
			width:400
		    });
	return false;
    });
    
    $(document).on("focus", "#dateFrom, #dateTo", function() {
	$(this).datepicker({ dateFormat: "dd/mm/yy" });
    });
    
    $(document).on("click", "#generate", function() {
	var from = $("#dateFrom").val();
	var to = $("#dateTo").val();
	$.colorbox.close();
	location.assign("{$_subdomain}/Report/jobsBooked?dateFrom=" + encodeURIComponent(from) + "&dateTo=" + encodeURIComponent(to));
    });


</script>

{/block}


{block name=body}

    
    <div class="breadcrumb">
	<div>
	     <a href="{$_subdomain}/SystemAdmin">{$page['Text']['main_page_title']|escape:'html'}</a>/ {$page['Text']['page_title']|escape:'html'}
	</div>
    </div>

    <div class="main" id="home" >

	<fieldset>
	    <legend>Reports</legend>
	    <a id="jobsBooked" href="{$_subdomain}/Report/jobsbooked">Jobs booked</a>
	</fieldset>

    </div>
    
    
{/block}
