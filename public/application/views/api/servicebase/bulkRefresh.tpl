{**
 * bulkRefresh.tpl
 * 
 * Allow the user enter a number of Skyline Job numbers seperated by commas and
 * perform a servicebase refresh on these.
 *
 * Called by the bulkSbRefreshAction method in the Jobs Controller
 *
 * @author     Andrew J. Williams <Andrew.Williams@awcomputech.com>
 * @copyright  2013 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.01
 *   
 * Changes
 * Date        Version Author                Reason
 * 15/05/2013  1.00    Andrew J. Williams    Initial Version
 * 17/05/2013  1.01    Andrew J. Williams    Failed Servicebase Refresh Jobs will list Service Centre
 ******************************************************************************}


{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "$Title - Servicebase Bulk Refresh"}
{$PageId = $BulkSbImport}
{$fullscreen=true}
{/block}

{block name=scripts}
{/block}

{block name=afterJqueryUI}
    <style>
        #JobNos { width: 100%; height: 400px; }
        #ResultText { width: 750px; height: 400px; }
        .ui-progressbar { position: relative; }
        .progress-label {
        position: absolute;
        width:100%;
        text-align: center;
        top: 4px;
        font-weight: bold;
        text-shadow: 1px 1px 0 #fff;
        }
    </style>
    <script>
        $(function() {
            $('#JobNos').resizable();
            $('#ResultText').resizable();
            $('#count').html('0');
            $("#ProgressDialogue").dialog({
                title: 'Bulk Servicebase Refresh',
                dialogClass: "no-close"
            });
            $("#ProgressDialogue").dialog('close');
            
            /////////
            $('#JobNos').live('input', function() {                             // On change of text box contents
                var jobs = $('#JobNos').val().split(",");                       // Split up text box contents by commas to get a list of Job Numbers
                if ( $('#JobNos').val().trim() == '' ) {                        // If text box is empty
                    $('#count').html('0');                                      // Then count must be 0
                } else {
                    if ( $('#JobNos').val().trim().substr(-1) == ',' ) {        // Otherwise check if the last character is a traing comma
                        $('#count').html(jobs.length - 1);                      // If so then count is one less   
                    } else {
                        $('#count').html(jobs.length);                          // If not then must be an entered Job No
                    } // fi == ','
                } // fi == ''
            });
            
            /////////
            $('#refresh').click( function() {                                   // On click refresh
                var n;                                                          // Loop counter
                var jobs = $('#JobNos').val().split(",");                       // Split up text box contents by commas to get a list of Job Numbers
                //var newtext = '';                                               // New text for the text area (for failed Jobs)
                
                $('#ResultText').val('');                                       // Clear any previous result

                $('#progressbar').progressbar({ max: $('#count').html()});      // Set progress bar max to number of jobs submitted
                $('#ProgressDialogue').dialog('open');                          // Display progress dialogue box
                var succ = 0;
                for (n = 0; n < jobs.length; ++n) {                             // Loop through each element in the array (which should be a job number
                    $('#progressbar').progressbar( "option", "value", n+1 );    // Update Progress bar
                    $('.progress-label').text('Processed '+$('#progressbar').progressbar('value')+ " of "+$('#count').html());
                    // Call appropriate controller
                    $.ajax( {
                        url:'{$_subdomain}/Job/sbSingleJobRefresh/'+jobs[n],
                        async: false,
                        success:function(data) {
                            var newtext = '';
                            var inx = data.indexOf(" has failed to refresh - ");
                             if ( inx == -1 )
                                succ++;
                            newtext = newtext +'SL Job No: '+$.trim(jobs[n]) + data + '\n';
                            $("#ResultText").val( $("#ResultText").val() + newtext); // Store result in results dialogue (colorbox)
                        }, // success
                        error:function(data){
                            newtext = newtext + jobs[n] + ',';
                        } // Error
                    }); // ajax
                } // next n
                var summary = "\nRequested: "+jobs.length+" Sucessful: "+succ;
                $("#ResultText").val( $("#ResultText").val() + summary);
                $("#ResultText").attr("readonly","true");
                $('#count').html('0');
                $("#JobNos").val("");
                /*$("#JobNos").val( newtext.replace(/(^,)|(,$)/g, "") );          // Remove leading or traling , from list of failed jobs.
                var leftjobs = $('#JobNos').val().split(",");                   // Split up text box contents by commas to get a list of Job Numbers
                if ( $('#JobNos').val().trim() == '' ) {                        // If text box is empty
                    $('#count').html('0');                                      // Then count must be 0
                } else {
                    $('#count').html(leftjobs.length);                          // If not then must be an entered Job Nos so update count
                }*/ // fi == ''
                $('.progress-label').text('Complete!');
                 
                $.colorbox({   
                    inline:		true,
                    href:		"#Result",
                    title:		'',
                    opacity:            0.75,
                    height:		200,
                    width:		900,
                    overlayClose:	false,
                    escKey:		false,
                    onComplete: function() {
                        $.colorbox.resize();
                    }
                }); 
                $("#ProgressDialogue").dialog('close');
            });
        });
    </script>
{/block}

{block name=body}
    
<div class="breadcrumb" {if $fullscreen==true}style="width:100%"{/if} >
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}
    </div>
</div>

{include file='include/menu.tpl'} 
    
<div class="main" id="home" {if $fullscreen==true}style="width:100%;min-width: 950px"{/if}>
    <div class="bulkRefreshPanel" {if $fullscreen==true}style="min-width: 950px;margin: auto"{/if}>		
        <fieldset>
            <legend title="">Servicebase Bulk Refresh Process</legend>
            <p>&nbsp;</p>
            <p>
                {$page['Text']['intro']|escape:'html'}
            </p>
            <p>&nbsp;</p>
            <textarea id='JobNos' name='JobNos'></textarea>
            <p>&nbsp;</p>
            <p>
                Count: <b><span id='count'></span></b> Skyline Job Numbers
                <input style="float:right;" type="submit" name="refresh" class="btnStandard" id="refresh" value="{$page['Buttons']['refresh']|escape:'html'}" />
            </p>
        </fieldset>
    </div>
</div>
            
<div id="ProgressDialogue">
    <p>&nbsp;</p>
    <div id="progressbar"><div class="progress-label">Loading...</div></div>
</div>

<div style="display:none;">          
    <div id="Result"  class="SystemAdminFormPanel">
        <textarea id="ResultText"></textarea>
        <p>
            <br/><br/>
            <span class="bottomButtons">
                <input type="submit" name="cancel_btn" class="btnCancel" id="cancel_btn" onclick="$.colorbox.close();" value="{$page['Buttons']['close']|escape:'html'}" />

            </span>
        </p>
    </div>
</div>
{/block}