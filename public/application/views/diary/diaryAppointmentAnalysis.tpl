<div>
    
    {if $diaryType=="FullViamente"}
        {$showTimeColumn="true"}
        {else}
             {$showTimeColumn="false"}
        {/if}
    <script>
        $(document).ready(function() {
        oTable2 = $('#summary_table').dataTable( {
"sDom": '<"top"f>t<"bottom"i><"clear">',
"bPaginate": false,
"aoColumns": [ 
			
			
			           
			  { "bVisible":    true },
			    null,
                            null,
                            null,
                            null,
                            null,
                            { "bVisible": {$showTimeColumn} },
                            
                           
                        null
                           
                            
                            
                            
		] 
});//end datatable
});//end document ready
    </script>  
    <div id="summary_tableDiv" style="{if $AutoDisplayTable=="Summary"&&(!isset($args.table)||$args.table=='0')}display:block{else}display:none{/if}">
    
 <table id="summary_table" border="0" cellpadding="0" cellspacing="0" class="browse" >
                 
                 <thead>
                     <tr>
                <th class="diaryDataTable" style="background-position:right bottom;">No.</th>
                <th class="diaryDataTable" style="background-position:right bottom;">DATE</th>
                <th class="diaryDataTable" style="background-position:right bottom;">SKILL TYPES</th>
                <th class="diaryDataTable" style="background-position:right bottom;">APPTS</th>
                <th  class="diaryDataTable" style="background-position:right bottom;">ENGINEERS</th>
                <th class="diaryDataTable" style="background-position:right bottom;">APPTS</th>
                <th class="diaryDataTable" style="background-position:right bottom;">HOURS</th>
                 <th  class="diaryDataTable" style="background-position:right bottom;">POSTCODES</th>
               
                </tr>
                </thead>
                <tbody>
        
                        
                     
                       {$order=1}
                    {foreach from=$summary key=k item=v}
                      
                        <tr>
                            <td class="diaryDataTable" style="width:20px">{$order}</td>
                      <td class="diaryDataTable" style="width:100px"><span style="font-weight:normal;font-size:12px;">{$k|date_format:"%A<br>  %e %B"}<br><br>Total {$v.total}<br>Appts</span></td>
                      <td class="diaryDataTable" style="width:200px;font-size:12px;" ><span style="font-weight:normal;font-size:12px;">{for $er=0 to $v.skills|@count-1}{$v['skills'][$er].skill}<br>{/for}</span></td>
                      <td class="diaryDataTable" style="width:50px;font-size:12px;"><span style="font-weight:normal;font-size:12px;">{for $er=0 to $v.skills|@count-1}{$v['skills'][$er].count}<br>{/for}</span></td>
                      <td class="diaryDataTable" style="width:200px"><span style="font-weight:normal;font-size:12px;">{for $er=0 to $v.engineers|@count-1} {if $v['engineers'][$er].name!=""}{if !isset($v['engineers'][$er].error)}{$v['engineers'][$er].name}{else} {if $allocationType!="GridMapping"}  <span style="color:red">{$v['engineers'][$er].name}</span>{else}{$v['engineers'][$er].name}{/if}{/if}{$skip[$er]=0}<br>{else}{$skip[$er]=1}{/if}{/for}{if $v.problem>0}<span style="color:red">ERROR</span>{/if}</span></td>
                      <td class="diaryDataTable" style="width:50px"><span style="font-weight:normal;font-size:12px;">{for $er=0 to $v.engineers|@count-1}{if $skip[$er]!=1}{if !isset($v['engineers'][$er].error)}{$v['engineers'][$er].appcount}{else}{if $allocationType!="GridMapping"}<span style="color:red">{$v['engineers'][$er].appcount}</span>{else}{$v['engineers'][$er].appcount}{/if}{/if}{if $v['engineers'][$er].appcount<10}&nbsp;&nbsp;&nbsp;{else}&nbsp;{/if}<br>{/if}{/for}{if $v.problem>0}<span style="color:red">{$v.problem}{/if}</span></span></td>
                     
                      <td class="diaryDataTable"><span style="font-weight:normal;font-size:12px;">{for $er=0 to $v.engineers|@count-1}{if $skip[$er]!=1}{if $v['engineers'][$er].appcount!=0}{$v['engineers'][$er].apptime|date_format:"%H:%M"}<br>{else}0<br>{/if}{/if}{/for}<span></td>
                     
                      <td class="diaryDataTable" style="width:200px"><span style="font-weight:normal;font-size:12px;">{for $er=0 to $v.postcode|@count-1}{$v['postcode'][$er].postcode},  {/for}</span></td>
                    </tr>
                   {$order=$order+1}
                  
                            {/foreach}
                 
                </tbody>
             </table>
</div>
                            </div>