<?php

/**
 * APILookups.class.php
 * 
 * Database access routines for the Lookup API
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       
 * @version    1.0
 */

require_once('CustomModel.class.php');

class APILookups extends CustomModel {
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
    
    /**
      * Description
      * 
      * Skiline API  Skyline.GetLookups
      * API Spec Section 1.1.4.9
      *  
      * Get client code and account number lookup
      * 
      * @param $UserId     UserID of branch                 
      * 
      * @return getNewJobs   Associative array containing recordset
      * 
      * @author Andrew Williams <a.williams@pccsuk.com>  
      */
    
    public function getLookups($UserID) {
        $sql = "
                SELECT
			clt.`AccountNumber` AS `ClientAccountNumber`,
			clt.`ClientName` AS `ClientName`			
		FROM
			`branch` b,
			`client` clt,
			`client_branch` clt_b,
			`user` u		
		WHERE
			u.`BranchID` = b.`BranchID`
			  AND b.`BranchID` = clt_b.`BranchID`
			    AND clt_b.`ClientID` = clt.`ClientID`
			AND u.`UserID` = $UserID					-- Authenticated user id
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result);

    }
    
    
    /**
      * Description
      * 
      * Skiline API  Skyline.GetLookups
      * API Spec Section 1.1.4.9
      *  
      * Get unit types for branch lookup
      * 
      * @param $UserId     UserID of branch                   
      * 
      * @return getUnitTypes   Associative array containing recordset
      * 
      * @author Andrew Williams <a.williams@pccsuk.com>  
      */
    
    public function getUnitTypes($UserID) {
        $sql = "
                SELECT DISTINCT
			`UnitTypeName` AS `ServiceTypeName`,
			jt.`Type` AS `JobType`
			
		FROM
			`branch` b,
			`client_branch` clt_b,
			`job_type` jt,
			`unit_client_type` ut_clt,
			`unit_type` ut,
			`user` u
		
		WHERE
			u.`BranchID` = clt_b.`BranchID`
			  AND clt_b.`ClientID` = ut_clt.`ClientID`
			    AND ut_clt.`UnitTypeID` = ut.`UnitTypeID`
			      AND ut.`JobTypeID` = jt.`JobTypeID`
			AND u.`UserID` = $UserID
                ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result);
    }
    
    
    /**
      * Description
      * 
      * Skiline API  Skyline.GetLookups
      * API Spec Section 1.1.4.9
      *  
      * Get service types for branch lookup
      * 
      * @param $UserId     UserID of branch                   
      * 
      * @return getServiceTypes   Associative array containing recordset
      * 
      * @author Andrew Williams <a.williams@pccsuk.com>  
      */
    
    public function getServiceTypes($UserID) {
        $sql = "
                SELECT DISTINCT
			st.`ServiceTypeName` AS ServiceTypeName,
			st.`Code` AS ServiceTypeCode,
			jt.`Type` AS `JobType`
		FROM
			`branch` b,
			`client_branch` clt_b,
			`job_type` jt,
			`service_type` st,
			`unit_client_type` ut_clt,
			`unit_type` ut,
			`user` u
		
		WHERE
			u.`BranchID` = clt_b.`BranchID`				-- Branch users have branch ID (otherwise NULL)
			  AND clt_b.`ClientID` = ut_clt.`ClientID`		-- Branch has clients
			    AND ut_clt.`UnitTypeID` = ut.`UnitTypeID`		-- Clients have unit types
			      AND ut.`JobTypeID` = jt.`JobTypeID`		-- Unit types have job types
			      AND ut.`JobTypeID` = st.`JobTypeID`		-- Job types have a service type
			AND u.`UserID` = $UserID                                -- Authenticated user
                ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result);
    }
    
    
    /**
      * Description
      * 
      * Skiline API  Skyline.GetLookups
      * API Spec Section 1.1.4.9
      *  
      * Get manufacturers for branch lookup
      * 
      * @param $UserId     UserID of branch                   
      * 
      * @return getManufacturers   Associative array containing recordset
      * 
      * @author Andrew Williams <a.williams@pccsuk.com>  
      */
    
    public function getManufacturers($UserID) {
        $sql = "
                SELECT DISTINCT
			mft.`ManufacturerName` AS `Manufacturer`
		FROM
			`branch` b,
			`client_branch` clt_b,
			`network_client` net_clt,
			`network_manufacturer` net_mft,
			`manufacturer` mft,
			`user` u
		
		WHERE
			u.`BranchID` = clt_b.`BranchID`					-- Branch users have branch ID (otherwise NULL)
			  AND clt_b.`ClientID` = net_clt.`ClientID`			-- Branch has clients
			    AND net_clt.`NetworkID` = net_mft.`NetworkID`		-- Client have networks
			      AND net_mft.`ManufacturerID` = mft.`ManufacturerID`	-- Network has manucturers
			AND u.`UserID` = $UserID                                        -- Authenticated user
                ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result);
    }    
    
}

?>