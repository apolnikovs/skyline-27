<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of System Statuses Page in Lookup Tables section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class MobilePhoneNetwork extends CustomModel {
    
    private $conn;
    private $dbColumns = array('T1.MobilePhoneNetworkID', 'T1.MobilePhoneNetworkName', 'T2.Name', 'T1.PhoneNetworkType', 'T1.Status');
    private $table     = "mobile_phone_network";
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    public function fetch($args) {
        
        
           $tables = $this->table." AS T1 LEFT JOIN country AS T2 ON T1.CountryID=T2.CountryID";
        
           $output = $this->ServeDataTables($this->conn, $tables, $this->dbColumns, $args);
        
        
           return  $output;
        
     }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         
         if(!isset($args['MobilePhoneNetworkID']) || !$args['MobilePhoneNetworkID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
     
    
    
    
    /**
     * Description
     * 
     * This method is used for to validate status name.
     *
     * @param interger $MobilePhoneNetworkName 
     * @param interger $CountryID  
     * @param interger $MobilePhoneNetworkID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($MobilePhoneNetworkName, $CountryID, $MobilePhoneNetworkID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT MobilePhoneNetworkID FROM '.$this->table.' WHERE MobilePhoneNetworkName=:MobilePhoneNetworkName AND CountryID=:CountryID AND MobilePhoneNetworkID!=:MobilePhoneNetworkID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':MobilePhoneNetworkName' => $MobilePhoneNetworkName, ':CountryID' => $CountryID, ':MobilePhoneNetworkID' => $MobilePhoneNetworkID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['MobilePhoneNetworkID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'INSERT INTO '.$this->table.' (MobilePhoneNetworkName, CountryID, PhoneNetworkType, Status, CreatedDate, ModifiedUserID, ModifiedDate)
            VALUES(:MobilePhoneNetworkName, :CountryID, :PhoneNetworkType, :Status, :CreatedDate, :ModifiedUserID, :ModifiedDate)';
        
        
        if($this->isValidAction($args['MobilePhoneNetworkName'], $args['CountryID'], 0))
        {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
            
            $insertQuery->execute(array(':MobilePhoneNetworkName' => $args['MobilePhoneNetworkName'], 
                ':CountryID' => $args['CountryID'], 
                ':PhoneNetworkType' => $args['PhoneNetworkType'], 
                ':Status' => $args['Status'], 
                ':CreatedDate' => date("Y-m-d H:i:s"),
                ':ModifiedUserID' => $this->controller->user->UserID,
                ':ModifiedDate' => date("Y-m-d H:i:s")
                
                ));
        
        
              return array('status' => 'OK',
                        'message' => 'Your data has been inserted successfully.');
        }
        else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT MobilePhoneNetworkID, MobilePhoneNetworkName, CountryID, PhoneNetworkType, Status, EndDate FROM '.$this->table.' WHERE MobilePhoneNetworkID=:MobilePhoneNetworkID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':MobilePhoneNetworkID' => $args['MobilePhoneNetworkID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to fetch all rows from database.
     *
     * @param int $CountryID  default false;
     * 
     * @global $this->table  
     * @return array It contains rows.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchAll($CountryID=false) {
        
        
        /* Execute a prepared statement by passing an array of values */
        if($CountryID)
        {    
            $sql = 'SELECT MobilePhoneNetworkID, MobilePhoneNetworkName FROM '.$this->table.' WHERE CountryID=:CountryID AND Status=:Status';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':CountryID' => $args['CountryID'], ':Status' => 'Active'));
        }
        else
        {
            $sql = 'SELECT MobilePhoneNetworkID, MobilePhoneNetworkName FROM '.$this->table.' WHERE Status=:Status';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':Status' => 'Active'));
        }
        $result = $fetchQuery->fetchAll();
        
        return $result;
    }
    
    
    
    
      /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['MobilePhoneNetworkName'], $args['CountryID'], $args['MobilePhoneNetworkID']))
        {        
            
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            if($this->controller->statuses[1]['Code']==$args['Status'])
            {
                if($row_data['Status']!=$args['Status'])
                {
                        $EndDate = date("Y-m-d H:i:s");
                }
                else
                {
                    $EndDate = $row_data['EndDate'];
                }    
            }
            
               /* Execute a prepared statement by passing an array of values */
              $sql = 'UPDATE '.$this->table.' SET 
                
              MobilePhoneNetworkName=:MobilePhoneNetworkName, CountryID=:CountryID, PhoneNetworkType=:PhoneNetworkType, Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate
              
              WHERE MobilePhoneNetworkID=:MobilePhoneNetworkID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(
                      
                      array(
                        
                        ':MobilePhoneNetworkName' => $args['MobilePhoneNetworkName'], 
                        ':CountryID' => $args['CountryID'], 
                        ':PhoneNetworkType' => $args['PhoneNetworkType'],  
                        ':Status' => $args['Status'],
                        ':EndDate' => $EndDate,
                        ':ModifiedUserID' => $this->controller->user->UserID,
                        ':ModifiedDate' => date("Y-m-d H:i:s"),
                        ':MobilePhoneNetworkID' => $args['MobilePhoneNetworkID']
                
                )
                      
             );
        
                
               return array('status' => 'OK',
                        'message' => 'Your data has been updated successfully.');
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => 'Your data has been deleted successfully.');
    }
    
    
}
?>