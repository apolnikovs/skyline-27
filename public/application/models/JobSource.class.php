<?php
/**
 * Part.class.php
 * 
 * This model handles interaction with the job source table.
 * 
 * @author      Andrew J. Williams <a.williams@pccsuk.com>
 * @version     1.0
 * @copyright   2013 PC Control Systems Ltd
 * 
 * Changes
 * Date        Version Author               Reason
 * 17/01/2013  1.00    Andrew J. Williams   Initial Version (created for Trackerbase VMS Log 136)
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class JobSource extends CustomModel {
    private $table;                                                             /* For Table Factory Class */
    public $debug = false;
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

        $this->table = TableFactory::JobSource();
    }
    
    /**
     * create
     *  
     * Create a job source
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new job source record
     * 
     * @return array    (status - Status Code, message - Status message, id - Id of inserted item)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        $cmd = $this->table->insertCommand( $args );
        $this->Execute($this->conn, $cmd, $args);
        
        $id = $this->conn->lastInsertId();
        
        if ( $id == 0 ) {                                                       /* No id of new insert so error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Created';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'id' => $id
                     )
               );
    }
    
    /**
     * update
     *  
     * Update a job source
     * 
     * @param array $args   Associative array of field values for to update the
     *                      job source. Not this must include the primary key 
     *                      (JobSourceID) for the record to be updated.
     * 
     * @return array    (status - Status Code, message - Status message, rows_affected number of rows updated)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function update($args) {
        $cmd = $this->table->updateCommand( $args );
        
        $rows_affected = $this->Execute($this->conn, $cmd, $args);
        
        if ( $rows_affected == 0 ) {                                             /* No rows affected may be error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Updated';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'rows_affected' => $rows_affected
                     )
               );
    }
    
    /**
     * delete
     *  
     * Delete a job source 
     * 
     * @param array $args (Field JobSourceID => Value ) for the item to be deleted
     * 
     * @return (status - Status Code, message - Status message, rows_affected 
     *         number of rows deleted)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function delete($args) {
        $index  = array_keys($args); 
        
        $cmd = $this->table->deleteCommand( $index[0].' = '.$args[$index[0]] );
        
        $rows_affected = $this->Execute($this->conn, $cmd, $args);
        
        if ( $rows_affected == 0 ) {                                             /* No rows affected may be error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Deleted';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'rows_affected' => $rows_affected
                     )
               );
    }
    
    /**
     * getIdFromName
     *  
     * Return the id of a specific job type
     * 
     * @param string $js    Job source
     * 
     * @return assoicative array with parts details
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getIdFromName($js) {
        $sql = "
                SELECT
			`JobSourceID`
		FROM
			`job_source`
		WHERE
			`Description` = '$js'
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['JobSourceID']);                                     /* Job source description exists - return id */
        } else {
            return(null);                                                       /* Not found return null */
        }
        
    }
    

}

?>
