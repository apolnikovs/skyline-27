<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Product Codes Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0
 */

class ProductCodes extends CustomModel {
    
   
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
   ////productcode functions 
    public function getProductCodeList($utID=false){
       
            $sql="select * from product_code where Status='Active'";
     
        $res=$this->query( $this->conn, $sql); 
        return $res;
    }
    
    
    public function insertProductCode($p){
        $sql="insert into product_code (ProductCodeName)
    values
    (:ProductCodeName)
    ";
        $params=array(
            'ProductCodeName'=>$p['ProductCodeName']
        
            
        );
       $this->execute( $this->conn, $sql,$params);  
    }
    
    public function updateProductCode($p){
          $sql="update product_code set ProductCodeName=:ProductCodeName, Status=:Status
    where ProductCodeID=:ProductCodeID;
    ";
        $params=array(
            'ProductCodeName'=>$p['ProductCodeName'],
            'Status'=>$p['Status'],
            
          
            'ProductCodeID'=>$p['ProductCodeID']
        );
       $this->execute( $this->conn, $sql,$params); 
    }
    
    public function getProductCodeData($id){
        $sql="select * from product_code where ProductCodeID=$id";
        $res=$this->query( $this->conn, $sql); 
        return $res[0];
    }
  
    public function deleteProductCode($id){
        $sql="update product_code set Status='In-Active' where ProductCodeID=$id";
        $this->execute( $this->conn, $sql); 
    }
    ////productcode functions 
    
    
    
    
    
}
?>