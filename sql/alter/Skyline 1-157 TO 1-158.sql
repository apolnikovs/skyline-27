# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.157');


# ---------------------------------------------------------------------- #
# Modify table "appointment"                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE `appointment` CHANGE COLUMN `Latitude` `Latitude` DECIMAL(17,14) NULL DEFAULT NULL AFTER `CompletionDateTime`;
ALTER TABLE `appointment` CHANGE COLUMN `Longitude` `Longitude` DECIMAL(17,14) NULL DEFAULT NULL AFTER `Latitude`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.158');




